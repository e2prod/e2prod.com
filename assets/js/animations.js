
window.sr = ScrollReveal();

sr.reveal('.articleLeft', {
    origin: 'top',
    distance: '50%',
    duration: 2000,
    delay: 50,
    scale: 0.75,
    rotate: {x: 90, y: 0, z: 0}
});

sr.reveal('.articleRight', {
    origin: 'bottom',
    distance: '50%',
    duration: 2000,
    delay: 50,
    scale: 0.75,
    rotate: {x: -90, y: 0, z: 0}
});

sr.reveal('.imgLeft', {
    origin: 'left',
    distance: '100%',
    duration: 2000,
    scale: 0.8
});

sr.reveal('.imgRight', {
    origin: 'right',
    distance: '100%',
    duration: 2000,
    scale: 0.8
});

sr.reveal('.contentLeft', {
    origin: 'left',
    duration: 2000
});

sr.reveal('.contentRight', {
    origin: 'right',
    duration: 2000
});

sr.reveal('.tecnologia', {
    distance: '130%',
    duration: 1500,
    scale: 0.5
}, 50);

sr.reveal('.clientes', {
    distance: '0px',
    duration: 2000,
    scale: 0.5
}, 50);

sr.reveal('.contactHeader', {
    origin: 'top',
    distance: '50%',
    duration: 2000
});

sr.reveal('.contact', {
    origin: 'left',
    distance: '35%',
    duration: 2000
});

sr.reveal('.form', {
    origin: 'right',
    distance: '35%',
    duration: 2000
});
