<?php
if ($_POST) {
  $ar = array_keys($_POST);
  $magic_number = $ar[3];
  if($magic_number == $_POST[$magic_number]) {
    
    $email_to = "info@e2prod.com";
    $email_subject = "Contacto desde e²prod.com";
    
    $name = $_POST['name'];         // required
    $email_from = $_POST['email'];  // required
    $message = $_POST['message'];   // required

    $email_message = "Detalles del formulario:\r\n\r\n";
    
    $email_message .= "Nombre: ".$name."\r\n";
    $email_message .= "Email: ".$email_from."\r\n";
    $email_message .= "Mensaje: ".$message."\r\n";

    // create email headers
    $headers = 'From: '.$email_from."\r\n".
    'Reply-To: '.$email_from."\r\n" .
    'X-Mailer: PHP/' . phpversion();
    
    if(!mail($email_to, $email_subject, $email_message, $headers)) {
      header('Location: contact_error.html');
    }
    
  } else {
    header('Location: contact_error.html');
  }
}
?>
<!DOCTYPE HTML>
<html>
  <head>
    <title>e²prod</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    <!--[if IE]><link rel="shortcut icon" href="images/favicon.ico"><![endif]-->
    <link rel="icon" href="images/favicon.png">
  </head>

  <body>
      <!-- Page Wrapper -->
      <div id="page-wrapper">

        <!-- Header -->
          <header id="header" class="alt">
            <!-- <h1><a href="index.html">e²prod</a></h1> -->
            <h1>
              <img id="logo" src="images/e2prodw.png" height="30em" alt="" /><br/>
            </h1>
            <nav>
              <a href="#menu">Menú</a>
            </nav>
          </header>

        <!-- Menu -->
          <nav id="menu">
            <div class="inner">
              <h2>Menú</h2>
              <ul class="links">
                <li><a href="/">Inicio</a></li>
              </ul>
              <a href="#" class="close">Cerrar</a>
            </div>
          </nav>
          
        <!-- Banner -->
          <section id="banner">
            <div class="inner">
              <div class="logo"><img src="images/e2prodw.png" height="70em" alt="" /></div>
              <h2>¡Gracias por contactarnos!</h2>
              <p>Responderemos tu consulta muy pronto</p>
              <a href="/" class="special">Volver</a>
            </div>
          </section>
          
    <!-- footer -->
      <section id="footer">
            <div class="inner">
              <ul class="copyright">
                <li>&copy; E²PROD SH. Todos los derechos reservados.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
              </ul>
            </div>
          </section>

      </div>

    <!-- Scripts -->
      <script src="assets/js/skel.min.js"></script>
      <script src="assets/js/jquery.min.js"></script>
      <script src="assets/js/jquery.scrollex.min.js"></script>
      <script src="assets/js/util.js"></script>
      <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
      <script src="assets/js/main.js"></script>

  </body>

</html>
