# e²prod.com

Código fuente de la página web de `e²prod sh` *todos los derechos reservados*.

## Colaborativo

Se agradece todo consejo o crítica sobre la página web, con este fin, pueden enviarnos
un mail a [info@e2prod.com](mailto:e2prod.com) con el **asunto**: "webpage".

## Aclaraciones legales

### Página web `e2prod.com`

La página web está hecha a base de un template de [HTML5 UP](http://html5up.net). Las
imágenes y fotos tiene *copyright* y su uso está prohibido sin concentimiento por parte 
nuestra y de las personas que aparecen en ellas.


### Template

Solid State by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


After a somewhat extended break from HTML5 UP (to work on a secret-ish new project --
more on that later!) I'm back with a brand new design: Solid State, a slick new multi-
pager that combines some of the ideas I've played with over at Pixelarity with an "angular"
sort of look. Hope you dig it :)

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = not included)

AJ
aj@lkn.io | @ajlkn


Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		background-size polyfill (github.com/louisremi)
		Misc. Sass functions (@HugoGiraudel)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)
